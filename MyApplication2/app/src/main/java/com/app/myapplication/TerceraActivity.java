package com.app.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TerceraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tercera);
    }

    // MéTODO DEL BOTÓN VOLVER AL INICIO

    public void VOLVERALINICIO (View view) {
        Intent volveralinicio = new Intent(this,MainActivity.class);
        startActivity(volveralinicio);
    }

}