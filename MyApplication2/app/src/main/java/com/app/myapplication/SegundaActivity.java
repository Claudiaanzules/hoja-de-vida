package com.app.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SegundaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
    }
    // Método para el Botón Siguiente
    public void SIGUIENTE (View view) {
        Intent siguiente = new Intent(this,TerceraActivity.class);
        startActivity(siguiente);

      }
}